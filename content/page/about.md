---
title: Qui suis-je ?
subtitle: Pourquoi on pourrait s'entendre et trainer ensemble
comments: false
---

Je m'appelle Pauline. Tu peux me trouver un surnom **sympa ET original** si tu veux (donc pas "Paupau" stp). Je suis étudiante en journalisme, mais tu me retrouveras peut-être dans 10 ans au Pérou à élever des Lamas. On verra. 
Si je devais te donner 3 qualités : 

- Chaque jours je valorise la bienveillance et l'écoute. 
- Des fois je suis drôle (donc SI, j'ai le sens de l'humour, mais si tu ne me fais pas rire, n'hésite pas à te remettre en question)
- J'aime les canards en plastique. j'en ai une centaine. 


### Mes réseaux

Qui suis-je, quels sont mes réseaux ? 
Tu peux essayer de me comprendre un peu plus sur mes réseaux
[Instagram](https://www.instagram.com/?hl=fr) et [Twitter](https://twitter.com/PolineBLANC)
**Tu comprendras tout**(quoique).
